# sentiment analysis ml

(Resum what is the project)

[[_TOC_]]

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Installing

After downloaded the project, we need install all the dependencies using [virtualenv](https://docs.python.org/3/library/venv.html), i am using linux, so the steps to install all the dependencies to run it is, if you use another operational system, please [click here to know more about it](https://docs.python.org/3/library/venv.html), after that run the command in your terminal at project folder.:

Command to create the virtualenv folder

```bat
$ python3 -m venv venv
```

Command to activate your virtualenv

```bat
$ source venv/bin/activate
```

Install all the dependencies at the project.

```bat
$ pip3 install -r requirements.txt
```

We are using [flask framework](https://flask.palletsprojects.com/en/1.1.x/) to execute the project, so the first command to run is:

```bat
$ export FLASK_APP=script.py
```

### Running

After do the instalations steps run this above command and a url (http://127.0.0.1:5000/) will be available to use the project

```bat
$ flask run
```

### Resorces avaliable

The first resource is /learn, this resource need be used before use the second resource /predict, because is the machine learn route, with this route the app will learn with de base and after that you are free to predict using the /predict route, look the example above:

**Curl request**

```bat
curl --location --request GET 'http://127.0.0.1:5000/learn'
```

**JSON response**

```jsonc
{
    accuracy: 0,82546372
}
```

The second resource is the route you will use when you want test the predictions, look the example above:

**Curl request**

```bat
curl --location --request POST 'http://127.0.0.1:5000/predict' \
--form 'phrases[]=i hate you' \
--form 'phrases[]=i love you' \
--form 'phrases[]=what is this?'
```


**JSON response**

```jsonc
{
    phrases: [
        {
            phrase: "i hate you",
            sentiment: "negative"
        },
        {
            phrase: "i love you",
            sentiment: "positive"
        },
        {
            phrase: "what is this?",
            sentiment: "neutral"
        }
    ]
}
```

## Explaining the code

(Explaining the code here)

### Explaining the module feature_extraction from package sklearn

Gathers utilities to build feature vectors from text documents. there are a lot of submodules like [CountVectorizer](https://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.CountVectorizer.html#sklearn.feature_extraction.text.CountVectorizer), [HashingVectorizer](https://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.HashingVectorizer.html#sklearn.feature_extraction.text.HashingVectorizer), [TfidfTransformer](https://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.TfidfTransformer.html#sklearn.feature_extraction.text.TfidfTransformer) and [TfidfVectorizer](https://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.TfidfVectorizer.html#sklearn.feature_extraction.text.TfidfVectorizer), let's see more about one witch other and the performance in the project.


See the accuracy by feature in the project:

| Feature name | Accuracy | Observations |
| ------ | ------ | ------ |
| TfidfVectorizer | 82.69% | 
| CountVectorizer | 82.53% | 
| HashingVectorizer | 82.59% | It takes too long |
| TfidfTransformer | None | Can't run using String data in pd.Series() |

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
