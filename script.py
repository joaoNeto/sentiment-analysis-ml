from sklearn.feature_extraction.text import TfidfVectorizer
from flask import Flask, request
import pandas as pd
import numpy as np
import sklearn
import pickle
import nltk 
import sys
import os
app = Flask(__name__)

FILE_MODEL = "model.sav"
FILE_VOCABULARY = "vocabulary.sav"

@app.route('/learn', methods=['GET'])
def learn():
  dataset = pd.read_csv("data.csv")
  dataset = dataset[dataset['selected_text'].str.len() > 2] # Removing phrases less than 2 characters

  STEMMER = nltk.stem.snowball.EnglishStemmer()
  STOPWORDS = nltk.corpus.stopwords.words('english')

  analyzer = sklearn.feature_extraction.text.TfidfVectorizer().build_analyzer()

  def stemmed_words(doc):
      return (STEMMER.stem(w) for w in analyzer(doc))

  vectorizer = sklearn.feature_extraction.text.TfidfVectorizer(
    stop_words=STOPWORDS # excluding stopwords
    ,analyzer=stemmed_words # applying english stemmer
    ,max_features=2000 # max numbers words that i will analyse
  )
  bag_of_words = vectorizer.fit_transform(dataset['selected_text'])

  training, test, classe_training, classe_test = sklearn.model_selection.train_test_split(bag_of_words, dataset['sentiment'], random_state = 42)

  logistic_regression = sklearn.linear_model.LogisticRegression( solver='lbfgs', max_iter=4000)
  logistic_regression.fit(training, classe_training)
  accuracy = logistic_regression.score(test, classe_test)

  # it's intersting apply 1vRest

  if os.path.exists(FILE_MODEL):
    os.remove(FILE_MODEL)
  if os.path.exists(FILE_VOCABULARY):
      os.remove(FILE_VOCABULARY)

  pickle.dump(logistic_regression,  open(FILE_MODEL, 'wb')) # saving the model ml
  pickle.dump(vectorizer.vocabulary_,open(FILE_VOCABULARY,"wb")) # saving the vocabulary

  return {"accuracy": accuracy}

@app.route('/predict', methods=['POST'])
def predict():
  model       = pickle.load(open(FILE_MODEL, 'rb'))
  vocabulary  = pickle.load(open(FILE_VOCABULARY, 'rb'))
  vectorizer  = TfidfVectorizer().fit(vocabulary)
  phrases     = request.form.getlist('phrases[]')
  results_ml  = model.predict(vectorizer.transform(phrases))
  items    = []

  for i in range(0,len(results_ml)):
    items.append({
      "phrase": phrases[i],
      "sentiment": results_ml[i]
    })

  return {"phrases": items}

